class Engine {
public :
    // actors is thhe list of all Actors on the map
    TCODList<Actor *> actors;
    Actor *player;
    Map *map;

    Engine();
    ~Engine();
    void update();
    void render();
};

// tells compiler that somewhere in .cpp there's a global named engine
extern Engine engine;
