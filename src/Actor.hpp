class Actor {
public :
    int x,y;
    // Uses int instead of char to store ch allowing for more than 256 character
    int ch;
    TCODColor col;

    // passing color as const to keep compiler from duplicating object before call
    Actor(int x, int y, int ch, const TCODColor &col);
    // const keyword after render() means fxn does not modify content of Actor object
    //  this allows to be called on constant objects
    void render() const;
};
