// Using struct as a data-only class
struct Tile {
    bool canWalk;   // can we walk through the tile?
    Tile() : canWalk(true) {}
};

class Map {
public :
    int width,height;

    Map(int width, int height);
    // declaration of destructor to release memory when Map object is destroyed
    ~Map();
    bool isWall(int x, int y) const;
    void render() const;
protected :
    Tile *tiles;
    
    void setWall(int x, int y);
};
